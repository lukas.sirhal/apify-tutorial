const Apify = require('apify');
const randomUA = require('modern-random-ua');
const WEB_URL = 'https://www.amazon.com';
const WEB_OFFER_URL = 'https://www.amazon.com/gp/offer-listing/';
const ASIN_OFFERS_COUNT = 'asin-offersCount'

const {log} = Apify.utils;
let asinOffers = {};

async function enqueuRequests(requestQueue, items, foreFront) {
  for (const item of items) {
    await requestQueue.addRequest(item, {foreFront});
  }
}

async function processOffers($, offersList, request, input, dataset) {
  const items = [];
  offersList.find('.olpOffer').each(function () {
    const item = {
      title: $('h1').text().trim(),
      itemUrl: request.userData.detail,
      asin: request.userData.asin,
      description: request.userData.description,
      keyword: input.keyword
    };
    const row = $(this);
    const seller = row.find('.olpSellerColumn > h3');
    if (seller.length > 0) {
      item.seller_name = seller.text().trim();
    }
    const offer = row.find('.olpOfferPrice');
    if (offer.length > 0) {
      item.offer = offer.text().trim();
    }
    items.push(item);
  });
  asinOffers[request.userData.asin] = items.length;
  await Apify.setValue(ASIN_OFFERS_COUNT, asinOffers);
  log.info(`Found ${items.length} offers`);
  await dataset.pushData(items);
}

Apify.main(async () => {
  const input = await Apify.getInput();
  if (!input.email) {
    throw new Error('Please, provide the email');
  }
  asinOffers = await Apify.getValue(ASIN_OFFERS_COUNT) || {};
  setInterval(() => {
    log.info('Offer counter:', asinOffers);
  }, 20000);

  const requestQueue = await Apify.openRequestQueue();
  const amazonSearchURL = `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=${input.keyword}`;
  // const dataset = await Apify.openDataset('amazon-tutorial-dataset');
  const dataset = await Apify.openDataset();
  await requestQueue.addRequest({
    url: amazonSearchURL,
    headers: {
      userAgent: randomUA.generate()
    },
    userData: {
      label: 'START'
    }
  });

  const handlePageFunction = async ({$, request}) => {
    if (request.userData.label === 'START') {
      log.info('Start scraping');
      await Apify.utils.sleep(300);
      const details = [];
      const results = $('.s-result-list > div');
      results.each(function () {
        const item = $(this);
        const data = {};
        data.asin = item.data('asin');
        if (data.asin !== undefined && data.asin.length > 0) {
          const links = item.find('span.rush-component > a');
          if (links.length > 0) {
            data.detail = $(links[0]).attr('href');
          }
          details.push({
            url: `${WEB_URL}${data.detail}`,
            headers: {
              userAgent: randomUA.generate()
            },
            userData: {
              label: 'DETAIL',
              asin: data.asin,
              detail: data.detail
            }
          });
        }
      });
      if(details.length === 0) {
        throw new Error('No products found');
      }
      log.info(`Found ${details.length} details`);
      await enqueuRequests(requestQueue, details, false);
    } else if (request.userData.label === 'DETAIL') {
      log.info(`Processing DETAIL page ${request.url}`);
      const description = $('#productDescription > p');
      if (description.length > 0) {
        const data = {
          url: `${WEB_OFFER_URL}${request.userData.asin}`,
          headers: {
            userAgent: randomUA.generate()
          },
          userData: {
            label: 'OFFERS',
            asin: request.userData.asin,
            description: description.text().trim(),
            detail: request.url
          }
        };
        await requestQueue.addRequest(data, {forefront: false});
      }
    } else if (request.userData.label === 'OFFERS') {
      log.info(`Processing ${request.url} offers`);
      const offersList = $('#olpOfferList');
      if (offersList.length > 0) {
        await processOffers($, offersList, request, input, dataset);

        const pages = $('ul.a-pagination');
        if (pages.length > 0) {
          const nextPages = [];
          pages.find('> li').each(function(){
            const li = $(this);
            if (!li.hasClass('a-disabled') && !li.hasClass('a-selected') && !li.hasClass('a-last')) {
              const href = li.find('> a').attr('href');
              nextPages.push({
                url: `${WEB_URL}${href}`,
                headers: {
                  userAgent: randomUA.generate()
                },
                userData: {
                  label: 'OFFERS_PAGE',
                  asin: request.userData.asin,
                  description: request.userData.description,
                  detail: request.userData.detail
                }
              });
            }
          });
          log.info(`Found ${nextPages.length} offers pages`);
          await enqueuRequests(requestQueue, nextPages, false);
        }
      }
    } else if (request.userData.label === 'OFFERS_PAGE') {
      log.info(`Processing ${request.url} offers page`);
      await Apify.utils.sleep(300);
      const offersList = $('#olpOfferList');
      if (offersList.length > 0) {
        await processOffers($, offersList, request, input, dataset);
      }
    }

  };

  const crawler = new Apify.CheerioCrawler({
    requestQueue,
    maxConcurrency: 20,
    maxOpenPagesPerInstance: 5,
    retireInstanceAfterRequestCount: 5,
    useApifyProxy: true,
    apifyProxyGroups: ['BUYPROXIES94952'],
    handlePageFunction,

    // This function is called if the page processing failed more than maxRequestRetries+1 times.
    handleFailedRequestFunction: async ({request}) => {
      console.log(`Request ${request.url} failed twice.`);
    },
  });

  await crawler.run();
  const env = await Apify.getEnv();
  log.info(JSON.stringify(env));
  const info = await dataset.getInfo();
  log.info(JSON.stringify(info));
  const datasetId = env.defaultDatasetId;
  if(datasetId !== undefined) {
    const text = `https://api.apify.com/v2/datasets/${datasetId}/items`;
    log.info('Sending email');
    await Apify.call('apify/send-mail', {
      to: input.email,
      cc: 'sirhallukas@gmail.com',
      subject: 'Lukas Sirhal This is for Apify SDK excercise',
      text: text
    });
  }
  console.log('Crawler finished.');
});
