# Tutorial ii apify sdk

#### Quiz

- Where and how can you use JQuery with the SDK?
    - The JQuery syntax can be used in CheerioCrawler
- What is the main difference between Cheerio and JQuery?
    - Cheerio is core API of JQuery and it's determined for node.js
- Where would you use CheerioCrawler and what are its limitations?
    - Cheerio should be use for simplier WEBs without require execute JS.
    - Limitations are in manipulation of website before scraping.
- What are the main classes for managing requests and when and why would you use one instead of another?
    - RequestQueue - becouse supports dynamic adding and removing of requests.
- How can you extract data from a page in Puppeteer without using JQuery?
    - With the page.$eval function
- What is the default concurrency/parallelism the SDK is running?
    - AutoscaledPool
