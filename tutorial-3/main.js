const Apify = require('apify');
const rp = require('request-promise');
const {log} = Apify.utils;

function getCleanPrice(price) {
  return parseFloat(price.replace('$', ''));
}

Apify.main(async () => {
  const input = await Apify.getInput();
  if (!input.datasetId) {
    throw new Error('Please, provide the email');
  }
  const options = {
    uri: `https://api.apify.com/v2/datasets/${input.datasetId}/items`,
    headers: {
      'Content-Type': 'application/json'
    },
    json: true
  };
  const items = [];
  await rp(options)
    .then(function (json) {
      log.info(`Data length ${json.length}`);
      const data = json.reduce(function (bestPriceProducts, product) {
        const asin = product.asin;
        if (!bestPriceProducts[asin]) {
          bestPriceProducts[asin] = product;
        } else {
          const oldPrice = getCleanPrice(bestPriceProducts[asin].offer);
          const newPrice = product.offer;
          if (oldPrice > newPrice) {
            bestPriceProducts[asin] = product;
          }
        }
        return bestPriceProducts;
      }, {});
      log.info('reduce finish');
      for(let k in data) {
        items.push(data[k]);
      }
    });
  await Apify.pushData(items);
  console.log('Crawler finished.');
});
