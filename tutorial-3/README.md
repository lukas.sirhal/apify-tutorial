# Tutorial III Apify actors & webhooks

#### Quiz

- How do you allocate more CPU for your actor run?
    - CPU's should be allocated by increasing memory (4096 MB of memory = 1 full CPU)
- How can you get the exact time when the actor was started from within the actor?
    - const run = await Apify.call(.... , so *run* include all information about external actor
    - Or if we have actor runID we can get data via API 
- What kinds of default storages an actor run gets allocated (connected to) by default?
    - key-value, dataset, request-queue
- Can you change the allocated memory of a running actor?
    - no
- How can you run an actor with Puppeteer on Apify with headful(non-headless) mode?
    - By using Chrominium instead common Chrom engine?
- Imagine the server/instance the container is running on has 32 GB and 8 cores CPU. What would the most performant (speed/cost) memory allocation for a CheerioCrawler? (hint: Node process cannot use user-created threads)
