# Tutorial VII Actor migrations and maintaining state

#### Quiz

- Actors have an option (in Settings) to be Restart on error. Would you use this for your regular actors? Why yes or not? And when would you use it and when not?
    - I guess this option might to be used carefully because there could be any exception wich could be thrown any time and it could cost too much. 
    So this option should be used with caution after you decided that actor really needed
- Migrations happen randomly but by setting Restart on error and then throwing an error in the main process, you can force a similar situation. Observe what happens. What changes and what is still the same for restarted actor run?
    - Migrations changes Id's of default storages?
- Why you usually don't need to add any special code for normal crawling/scraping to manage to handle migrations? Is there any component that basically solves this problem for you?
    - Because the state is persisted in to the apify_storage during scraping and this storage is handled across instances.
- How can you intercept the migration event? How much time do you after this event happens and before the actor migrates?
    - Listening Apify.events...
- When would you persist data to default key-value store and when rather to the named key-value store?
    - When i would don't need share this store across noncontinuous actor chain
