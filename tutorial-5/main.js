const Apify = require('apify');
const ApifyClient = require('apify-client');
const rp = require('request-promise');
const TOKEN = '******************';
const TASK_ID = 'zPXEh5NyvKqcWexHA';
const INPUT = {
  "keyword": "phone",
  "email": "lukas@apify.com"
};

const {log} = Apify.utils;
log.setLevel(log.LEVELS.WARNING);

async function runTaskViaClient(memory, limit, fields) {
  const apifyClient = new ApifyClient({
    userId: 'eFWXxAQdvDBnhmbay',
    token: TOKEN
  });
  const actRun = await apifyClient.tasks.runTask({
    taskId: TASK_ID,
    memory: memory,
    waitForFinish: 120,
    input: INPUT
  });
  const datasets = await apifyClient.datasets;
  const data = await datasets.getItems({
    datasetId: actRun.defaultDatasetId,
    limit: limit,
    fields: fields,
    format: 'csv'
  });
  return data.items;
}

async function runTaskViaAPI(memory, limit, fields) {
  const options = {
    method: 'POST',
    uri: `https://api.apify.com/v2/actor-tasks/${TASK_ID}/runs?token=${TOKEN}&memory=${memory}&waitForFinish=160`,
    headers: {
      'Content-Type': 'application/json'
    },
    json: true,
    body: INPUT
  };
  let datasetId = null;
  await rp(options)
    .then(function (json) {
      datasetId = json.data.defaultDatasetId;
    });
  const optionsData = {
    method: 'GET',
    uri: `https://api.apify.com/v2/datasets/${datasetId}/items?token=${TOKEN}&format=csv&limit=${limit}&fields=${fields}`,
    headers: {
      'Content-Type': 'application/json'
    },
    json: true,
  };
  return await rp(optionsData);
}

Apify.main(async () => {
  const input = await Apify.getInput();
  let data = [];
  if (input.useClient) {
    data = await runTaskViaClient(input.memory, input.maxItems, input.fields);
  } else {
    data = await runTaskViaAPI(input.memory, input.maxItems, input.fields)
  }
  await Apify.setValue('OUTPUT', data, { contentType: 'text/csv'});
  console.log('Crawler finished.');
});
