# Tutorial V Tasks, storage, API & client

#### Quiz

- What is the relationship between actor and task?
    - Task are only configuration facade of actors.
    - You can also create task for actor which has been made by someone else.
    - When you run task you run an anctor with differend configuration.
- What are the differences between default (unnamed) and named storage? Which one would you use for everyday usage?
    - Default storage has been generated in every actor run while named storage was created at once. So
    if you want have same data across multiple actors, you can simply create named dataset and his name use in every actor. 
    - Unnamed dataset has only **datasetId**.
- What is the relationship between Apify API and Apify client? Are there any significant differences?
    - Apify-client is JS library for apify API. And it's better use library in code then HTTP requests on API.
- Is it possible to use a request queue for deduplication of product ids? If yes, how would you do that?
    - Yes, if request is already in the queue then addRequest returns info about that request.
- What is data retention and how does it work for all types of storages (default and named)?
    - How long we're going to store results of your crawlers and actors in the database.
- How do you pass input when running actor or task via API?
    - Via request body
