# tutorial vi proxy and bypassing antiscraping software

#### Quiz

- What are the types of proxies of Apify proxy? What are the main differences between them?
    - **Datacenter proxy servers:** Simply automatically rotate ip addresses available to the user. For each HTTP/S request 
    proxy takes the list of IP addresses and chose one that has been used the longest time ago.
    - **Residential proxy:** It works like datacenter proxy but with more IPs - it depends on subscription plan
    - **Google SERP proxy:** This proxy was made for google search results
- What proxies (proxy groups) do users get with the proxy trial? How long this trial lasts?
    - At pricing table are 30 shared datacenter proxies for one month and 100 queries to Google SERPs
- How can you prevent an error occurring if one of the proxy groups that a user has will be removed? What should be the best practice?
    - Use more proxy groups?
- Does it make sense to rotate proxy when you are logged in?
    - It's depends if the site has any detection too many requests by IP.
- Construct a proxy URL that will select proxies only from the US.
    - http://country-US:@proxy.apify.com:8000
- What do you need to do to rotate a proxy(one proxy usually has one IP)? How does this differ for CheerioCrawler and PuppeteerCrawler?
    - **Puppeteer**: via launch options
    - **Cheerio**: via initialize options
- Try to set up Apify proxy (any group or auto) in your browser. This is useful for testing how websites behave with the proxy from a specific country (most are from the US). Were you successful?
    - nope, Ubuntu, Chrome 77
    - I remembered that we use proxy for access some game which was disabled for CR :-)
- Name a few different ways of how a website can prevent you from scraping it.
    - IP detection, IP rate limiting, Browser detection, Tracking user behavior
- Do you know some software companies that are developing anti-scraping solutions? Have you ever encountered them on the website?
    - I don't know any but i tried google it and after some links with "How to getting blocked while scraping..." 
    i got to one article with this header: "Anti-Scraping – Why You Already Lost The War"
