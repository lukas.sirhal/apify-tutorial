# Tutorial IV Apify CLI & source code

#### Quiz

- Do you have to rebuild an actor each time the source code is changed?
    - yes
- What is the difference between pushing the code changes and creating pull request?
    - If you create pull request other devs can comment your work, so there can be found bugs which you can overlook be yourself.
- How does apify push command work? Is it worth to use it in your opinion?
    - Apify zip your project to archive and uploud it to the cloud. 
    - It depends on you have your code in some git repository with hooks or if you have only local development with apify-cli connect. 
      And because push will build your actor you have to be sure about your changes of course.
